package tree;

import java.util.Scanner;

/**
 * The tester method for solving an 8 tile puzzle
 * @author Brian Anello
 */
public class Tester {
    public static void main(String [] args) {
        if(args.length > 0 && args[0].equals("-v") == true) {
            Scanner scan = new Scanner(System.in);
            String x, y;
            System.out.println("Please enter the initial state of the puzzle in this form: 123456780");
            System.out.println("0 acts as the space or blank piece");
            x = scan.next();
            System.out.println("Please enter the goal state you wish to get. To check for solvability in the beginning the goal state must be 123456780 to function properly");
            y = scan.next();
            PuzzleTree pt = new PuzzleTree(x, y);
            System.out.println("Is it solvable: " + pt.root().isSolvable());
            if(pt.root().isSolvable() == false) {
                System.out.println("Do you wish to continue anyways? y for yes / anything else for no");
                x = scan.next();
                if(x.equals("y") == true) {                    
                    System.out.println("All nodes until limit reached or answer found:");
                    String str = pt.findAnsV();
                    System.out.println("Answer: " + str);
                }
            } else {
                System.out.println("All nodes until limit reached or answer found:");
                String str = pt.findAnsV();
                System.out.println("Answer: " + str);
            }
        } else {
            Scanner scan = new Scanner(System.in);
            String x, y;
            System.out.println("Please enter the initial state of the puzzle in this form: 123456780");
            System.out.println("0 acts as the space or blank piece");
            x = scan.next();
            System.out.println("Please enter the goal state you wish to get. To check for solvability in the beginning the goal state must be 123456780 to function properly");
            y = scan.next();
            PuzzleTree pt = new PuzzleTree(x, y);
            System.out.println("Is it solvable: " + pt.root().isSolvable());
            if(pt.root().isSolvable() == false) {
                System.out.println("Do you wish to continue anyways? y for yes / anything else for no");
                x = scan.next();
                if(x.equals("y") == true) {  
                    String str = pt.findAns();
                    if(pt.root().isSolvable() == true) System.out.println("Moves needed to reach goal state with the depth specified in PuzzleTree class of " + pt.depth() + ": " + str);
                }
            } else {
                String str = pt.findAns();
                if(pt.root().isSolvable() == true) System.out.println("Moves needed to reach goal state with the depth specified in PuzzleTree class of " + pt.depth() + ": " + str);
            }
        }
    }
}
